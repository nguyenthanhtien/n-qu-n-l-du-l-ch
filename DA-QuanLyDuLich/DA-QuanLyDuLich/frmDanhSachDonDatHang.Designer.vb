﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDanhsachdondathang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgv_danhsachdonhang = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_chinhsua = New System.Windows.Forms.Button()
        Me.btn_thongtinkhachhang = New System.Windows.Forms.Button()
        Me.btn_taothongtin = New System.Windows.Forms.Button()
        Me.btn_huydonhang = New System.Windows.Forms.Button()
        Me.cb_timkiem = New System.Windows.Forms.ComboBox()
        Me.txt_timkiem = New System.Windows.Forms.TextBox()
        Me.lb_ngaytao = New System.Windows.Forms.Label()
        Me.dtp_ngaytao = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        CType(Me.dgv_danhsachdonhang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv_danhsachdonhang
        '
        Me.dgv_danhsachdonhang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_danhsachdonhang.Location = New System.Drawing.Point(17, 19)
        Me.dgv_danhsachdonhang.Name = "dgv_danhsachdonhang"
        Me.dgv_danhsachdonhang.Size = New System.Drawing.Size(653, 212)
        Me.dgv_danhsachdonhang.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_huydonhang)
        Me.GroupBox1.Controls.Add(Me.btn_taothongtin)
        Me.GroupBox1.Controls.Add(Me.btn_thongtinkhachhang)
        Me.GroupBox1.Controls.Add(Me.btn_chinhsua)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 349)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(676, 84)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tùy chỉnh"
        '
        'btn_chinhsua
        '
        Me.btn_chinhsua.Location = New System.Drawing.Point(23, 27)
        Me.btn_chinhsua.Name = "btn_chinhsua"
        Me.btn_chinhsua.Size = New System.Drawing.Size(90, 44)
        Me.btn_chinhsua.TabIndex = 0
        Me.btn_chinhsua.Text = "Chỉnh sửa đơn hàng"
        Me.btn_chinhsua.UseVisualStyleBackColor = True
        '
        'btn_thongtinkhachhang
        '
        Me.btn_thongtinkhachhang.Location = New System.Drawing.Point(193, 27)
        Me.btn_thongtinkhachhang.Name = "btn_thongtinkhachhang"
        Me.btn_thongtinkhachhang.Size = New System.Drawing.Size(90, 44)
        Me.btn_thongtinkhachhang.TabIndex = 1
        Me.btn_thongtinkhachhang.Text = "Thông tin khách hàng"
        Me.btn_thongtinkhachhang.UseVisualStyleBackColor = True
        '
        'btn_taothongtin
        '
        Me.btn_taothongtin.Location = New System.Drawing.Point(360, 27)
        Me.btn_taothongtin.Name = "btn_taothongtin"
        Me.btn_taothongtin.Size = New System.Drawing.Size(82, 39)
        Me.btn_taothongtin.TabIndex = 2
        Me.btn_taothongtin.Text = "Tạo thông tin khách hàng"
        Me.btn_taothongtin.UseVisualStyleBackColor = True
        '
        'btn_huydonhang
        '
        Me.btn_huydonhang.Location = New System.Drawing.Point(522, 27)
        Me.btn_huydonhang.Name = "btn_huydonhang"
        Me.btn_huydonhang.Size = New System.Drawing.Size(84, 39)
        Me.btn_huydonhang.TabIndex = 3
        Me.btn_huydonhang.Text = "Hủy đơn hàng"
        Me.btn_huydonhang.UseVisualStyleBackColor = True
        '
        'cb_timkiem
        '
        Me.cb_timkiem.FormattingEnabled = True
        Me.cb_timkiem.Items.AddRange(New Object() {"Tên khách hàng", "Số điện thoại ", "Tên Tour", "Trạng thái"})
        Me.cb_timkiem.Location = New System.Drawing.Point(16, 25)
        Me.cb_timkiem.Name = "cb_timkiem"
        Me.cb_timkiem.Size = New System.Drawing.Size(121, 21)
        Me.cb_timkiem.TabIndex = 4
        '
        'txt_timkiem
        '
        Me.txt_timkiem.Location = New System.Drawing.Point(163, 25)
        Me.txt_timkiem.Name = "txt_timkiem"
        Me.txt_timkiem.Size = New System.Drawing.Size(100, 20)
        Me.txt_timkiem.TabIndex = 5
        '
        'lb_ngaytao
        '
        Me.lb_ngaytao.AutoSize = True
        Me.lb_ngaytao.Location = New System.Drawing.Point(337, 28)
        Me.lb_ngaytao.Name = "lb_ngaytao"
        Me.lb_ngaytao.Size = New System.Drawing.Size(53, 13)
        Me.lb_ngaytao.TabIndex = 6
        Me.lb_ngaytao.Text = "Ngày tạo:"
        '
        'dtp_ngaytao
        '
        Me.dtp_ngaytao.Location = New System.Drawing.Point(396, 28)
        Me.dtp_ngaytao.Name = "dtp_ngaytao"
        Me.dtp_ngaytao.Size = New System.Drawing.Size(200, 20)
        Me.dtp_ngaytao.TabIndex = 7
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtp_ngaytao)
        Me.GroupBox2.Controls.Add(Me.lb_ngaytao)
        Me.GroupBox2.Controls.Add(Me.txt_timkiem)
        Me.GroupBox2.Controls.Add(Me.cb_timkiem)
        Me.GroupBox2.Location = New System.Drawing.Point(32, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(625, 65)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tìm kiếm"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgv_danhsachdonhang)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 88)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(690, 244)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Danh sách đơn hàng"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'frmDanhsachdondathang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 463)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmDanhsachdondathang"
        Me.Text = "Danh sách đơn đặt hàng"
        CType(Me.dgv_danhsachdonhang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgv_danhsachdonhang As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btn_huydonhang As Button
    Friend WithEvents btn_taothongtin As Button
    Friend WithEvents btn_thongtinkhachhang As Button
    Friend WithEvents btn_chinhsua As Button
    Friend WithEvents cb_timkiem As ComboBox
    Friend WithEvents txt_timkiem As TextBox
    Friend WithEvents lb_ngaytao As Label
    Friend WithEvents dtp_ngaytao As DateTimePicker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ColorDialog1 As ColorDialog
End Class
