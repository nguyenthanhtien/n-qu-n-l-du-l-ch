﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmThongTinDonDatHang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tbTenTour = New System.Windows.Forms.TextBox()
        Me.tbTenKhachHang = New System.Windows.Forms.TextBox()
        Me.tbDiaChi = New System.Windows.Forms.TextBox()
        Me.tbSoDienThoai = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbGioiTinh = New System.Windows.Forms.TextBox()
        Me.tbCMND = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FontDialog1 = New System.Windows.Forms.FontDialog()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 34)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(476, 384)
        Me.DataGridView1.TabIndex = 0
        '
        'tbTenTour
        '
        Me.tbTenTour.Location = New System.Drawing.Point(518, 34)
        Me.tbTenTour.Name = "tbTenTour"
        Me.tbTenTour.Size = New System.Drawing.Size(285, 20)
        Me.tbTenTour.TabIndex = 1
        '
        'tbTenKhachHang
        '
        Me.tbTenKhachHang.Location = New System.Drawing.Point(518, 112)
        Me.tbTenKhachHang.Name = "tbTenKhachHang"
        Me.tbTenKhachHang.Size = New System.Drawing.Size(285, 20)
        Me.tbTenKhachHang.TabIndex = 1
        '
        'tbDiaChi
        '
        Me.tbDiaChi.Location = New System.Drawing.Point(518, 313)
        Me.tbDiaChi.Name = "tbDiaChi"
        Me.tbDiaChi.Size = New System.Drawing.Size(285, 20)
        Me.tbDiaChi.TabIndex = 1
        '
        'tbSoDienThoai
        '
        Me.tbSoDienThoai.Location = New System.Drawing.Point(518, 384)
        Me.tbSoDienThoai.Name = "tbSoDienThoai"
        Me.tbSoDienThoai.Size = New System.Drawing.Size(285, 20)
        Me.tbSoDienThoai.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(516, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Tên tour"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(515, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tên khách hàng"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(516, 297)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Địa chỉ"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(516, 368)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Số điện thoại"
        '
        'tbGioiTinh
        '
        Me.tbGioiTinh.Location = New System.Drawing.Point(518, 184)
        Me.tbGioiTinh.Name = "tbGioiTinh"
        Me.tbGioiTinh.Size = New System.Drawing.Size(285, 20)
        Me.tbGioiTinh.TabIndex = 1
        '
        'tbCMND
        '
        Me.tbCMND.Location = New System.Drawing.Point(518, 246)
        Me.tbCMND.Name = "tbCMND"
        Me.tbCMND.Size = New System.Drawing.Size(285, 20)
        Me.tbCMND.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(515, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Giới tính"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(515, 230)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "CMND"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Thông tin khách hàng"
        '
        'frmThongTinDonDatHang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 416)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbCMND)
        Me.Controls.Add(Me.tbSoDienThoai)
        Me.Controls.Add(Me.tbGioiTinh)
        Me.Controls.Add(Me.tbDiaChi)
        Me.Controls.Add(Me.tbTenKhachHang)
        Me.Controls.Add(Me.tbTenTour)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "frmThongTinDonDatHang"
        Me.Text = "frmThongTinDonDatHang"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents tbTenTour As TextBox
    Friend WithEvents tbTenKhachHang As TextBox
    Friend WithEvents tbDiaChi As TextBox
    Friend WithEvents tbSoDienThoai As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents tbGioiTinh As TextBox
    Friend WithEvents tbCMND As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents FontDialog1 As FontDialog
End Class
