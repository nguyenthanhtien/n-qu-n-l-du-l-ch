﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMHChinhNV
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ChứcNăngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐặtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThôngTinChiTiếtToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DịchVụToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChiTiếtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NhàHàngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PhươngTiệnDiChuyểnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TạoTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TạoTourRiêngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýNhânViênToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTỉnhThànhToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýĐiểmDuLịchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýKháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChứcNăngToolStripMenuItem, Me.QuảnLýToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(697, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ChứcNăngToolStripMenuItem
        '
        Me.ChứcNăngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ĐặtTourToolStripMenuItem, Me.ThôngTinChiTiếtToolStripMenuItem, Me.DịchVụToolStripMenuItem, Me.ChiTiếtTourToolStripMenuItem})
        Me.ChứcNăngToolStripMenuItem.Name = "ChứcNăngToolStripMenuItem"
        Me.ChứcNăngToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ChứcNăngToolStripMenuItem.Text = "Chức năng"
        '
        'ĐặtTourToolStripMenuItem
        '
        Me.ĐặtTourToolStripMenuItem.Name = "ĐặtTourToolStripMenuItem"
        Me.ĐặtTourToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ĐặtTourToolStripMenuItem.Text = "Đặt Tour"
        '
        'ThôngTinChiTiếtToolStripMenuItem
        '
        Me.ThôngTinChiTiếtToolStripMenuItem.Name = "ThôngTinChiTiếtToolStripMenuItem"
        Me.ThôngTinChiTiếtToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ThôngTinChiTiếtToolStripMenuItem.Text = "Cập nhật thông tin"
        '
        'DịchVụToolStripMenuItem
        '
        Me.DịchVụToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KháchSạnToolStripMenuItem, Me.NhàHàngToolStripMenuItem, Me.PhươngTiệnDiChuyểnToolStripMenuItem})
        Me.DịchVụToolStripMenuItem.Name = "DịchVụToolStripMenuItem"
        Me.DịchVụToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.DịchVụToolStripMenuItem.Text = "Dịch vụ"
        '
        'ChiTiếtTourToolStripMenuItem
        '
        Me.ChiTiếtTourToolStripMenuItem.Name = "ChiTiếtTourToolStripMenuItem"
        Me.ChiTiếtTourToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ChiTiếtTourToolStripMenuItem.Text = "Chi tiết Tour"
        '
        'KháchSạnToolStripMenuItem
        '
        Me.KháchSạnToolStripMenuItem.Name = "KháchSạnToolStripMenuItem"
        Me.KháchSạnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.KháchSạnToolStripMenuItem.Text = "Khách sạn"
        '
        'NhàHàngToolStripMenuItem
        '
        Me.NhàHàngToolStripMenuItem.Name = "NhàHàngToolStripMenuItem"
        Me.NhàHàngToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.NhàHàngToolStripMenuItem.Text = "Nhà hàng"
        '
        'PhươngTiệnDiChuyểnToolStripMenuItem
        '
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Name = "PhươngTiệnDiChuyểnToolStripMenuItem"
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Text = "Phương tiện di chuyển"
        '
        'QuảnLýToolStripMenuItem
        '
        Me.QuảnLýToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TạoTourToolStripMenuItem, Me.QuảnLýTourToolStripMenuItem, Me.TạoTourRiêngToolStripMenuItem, Me.QuảnLýToolStripMenuItem1, Me.QuảnLýNhânViênToolStripMenuItem, Me.QuảnLýTỉnhThànhToolStripMenuItem, Me.QuảnLýĐiểmDuLịchToolStripMenuItem, Me.QuảnLýKháchSạnToolStripMenuItem, Me.QuảnLýToolStripMenuItem2})
        Me.QuảnLýToolStripMenuItem.Name = "QuảnLýToolStripMenuItem"
        Me.QuảnLýToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.QuảnLýToolStripMenuItem.Text = "Quản lý"
        '
        'TạoTourToolStripMenuItem
        '
        Me.TạoTourToolStripMenuItem.Name = "TạoTourToolStripMenuItem"
        Me.TạoTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TạoTourToolStripMenuItem.Text = "Tạo Tour"
        '
        'QuảnLýTourToolStripMenuItem
        '
        Me.QuảnLýTourToolStripMenuItem.Name = "QuảnLýTourToolStripMenuItem"
        Me.QuảnLýTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.QuảnLýTourToolStripMenuItem.Text = "Quản lý Tour"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(123, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Số lượng người : "
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(123, 77)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(49, 20)
        Me.TextBox1.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Chọn Tour :"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(123, 148)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox2.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Quán ăn :"
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(123, 115)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox3.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 118)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Khách sạn :"
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(123, 181)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox4.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Phương tiện : "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 218)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "ID Hướng dẫn viên :"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(123, 215)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(121, 20)
        Me.TextBox2.TabIndex = 3
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(14, 241)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(671, 150)
        Me.DataGridView1.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(279, 214)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Thêm"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(473, 213)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Sữa"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(322, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Chi phí :"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(396, 43)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(121, 20)
        Me.TextBox3.TabIndex = 3
        '
        'ComboBox5
        '
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(396, 80)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox5.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(322, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Thanh toán :"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(378, 212)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Xóa"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TạoTourRiêngToolStripMenuItem
        '
        Me.TạoTourRiêngToolStripMenuItem.Name = "TạoTourRiêngToolStripMenuItem"
        Me.TạoTourRiêngToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.TạoTourRiêngToolStripMenuItem.Text = "Tạo Tour riêng"
        '
        'QuảnLýToolStripMenuItem1
        '
        Me.QuảnLýToolStripMenuItem1.Name = "QuảnLýToolStripMenuItem1"
        Me.QuảnLýToolStripMenuItem1.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýToolStripMenuItem1.Text = "Quản lý khách hàng"
        '
        'QuảnLýNhânViênToolStripMenuItem
        '
        Me.QuảnLýNhânViênToolStripMenuItem.Name = "QuảnLýNhânViênToolStripMenuItem"
        Me.QuảnLýNhânViênToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýNhânViênToolStripMenuItem.Text = "Quản lý nhân viên"
        '
        'QuảnLýTỉnhThànhToolStripMenuItem
        '
        Me.QuảnLýTỉnhThànhToolStripMenuItem.Name = "QuảnLýTỉnhThànhToolStripMenuItem"
        Me.QuảnLýTỉnhThànhToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýTỉnhThànhToolStripMenuItem.Text = "Quản lý tỉnh thành"
        '
        'QuảnLýĐiểmDuLịchToolStripMenuItem
        '
        Me.QuảnLýĐiểmDuLịchToolStripMenuItem.Name = "QuảnLýĐiểmDuLịchToolStripMenuItem"
        Me.QuảnLýĐiểmDuLịchToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýĐiểmDuLịchToolStripMenuItem.Text = "Quản lý điểm du lịch"
        '
        'QuảnLýKháchSạnToolStripMenuItem
        '
        Me.QuảnLýKháchSạnToolStripMenuItem.Name = "QuảnLýKháchSạnToolStripMenuItem"
        Me.QuảnLýKháchSạnToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýKháchSạnToolStripMenuItem.Text = "Quản lý khách sạn"
        '
        'QuảnLýToolStripMenuItem2
        '
        Me.QuảnLýToolStripMenuItem2.Name = "QuảnLýToolStripMenuItem2"
        Me.QuảnLýToolStripMenuItem2.Size = New System.Drawing.Size(184, 22)
        Me.QuảnLýToolStripMenuItem2.Text = "Quản lý nhà hàng"
        '
        'frmMHChinhNV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 408)
        Me.Controls.Add(Me.ComboBox5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMHChinhNV"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ChứcNăngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ĐặtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThôngTinChiTiếtToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DịchVụToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NhàHàngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PhươngTiệnDiChuyểnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChiTiếtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TạoTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ComboBox3 As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ComboBox4 As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents ComboBox5 As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents TạoTourRiêngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents QuảnLýNhânViênToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýTỉnhThànhToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýĐiểmDuLịchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýKháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýToolStripMenuItem2 As ToolStripMenuItem
End Class
