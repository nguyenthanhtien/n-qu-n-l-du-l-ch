﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDanhSachTour
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ChứcNăngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐặtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CậpNhậtThôngTinToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThêmKháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NhàHàngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PhươngTiệnDiChuyểnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.XemChiTiếtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThêmTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.XóaTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(7, 82)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(679, 205)
        Me.DataGridView1.TabIndex = 0
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(15, 15)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Tên", "Giá", "Địa điểm", "Ngày tạo"})
        Me.ComboBox1.Location = New System.Drawing.Point(135, 13)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 4
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(274, 14)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Search"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(363, 42)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tìm kiếm"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChứcNăngToolStripMenuItem, Me.QuảnLýToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(694, 24)
        Me.MenuStrip1.TabIndex = 7
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ChứcNăngToolStripMenuItem
        '
        Me.ChứcNăngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ĐặtTourToolStripMenuItem, Me.CậpNhậtThôngTinToolStripMenuItem, Me.ThêmKháchSạnToolStripMenuItem, Me.XemChiTiếtTourToolStripMenuItem})
        Me.ChứcNăngToolStripMenuItem.Name = "ChứcNăngToolStripMenuItem"
        Me.ChứcNăngToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ChứcNăngToolStripMenuItem.Text = "Chức năng"
        '
        'ĐặtTourToolStripMenuItem
        '
        Me.ĐặtTourToolStripMenuItem.Name = "ĐặtTourToolStripMenuItem"
        Me.ĐặtTourToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ĐặtTourToolStripMenuItem.Text = "Đặt tour"
        '
        'CậpNhậtThôngTinToolStripMenuItem
        '
        Me.CậpNhậtThôngTinToolStripMenuItem.Name = "CậpNhậtThôngTinToolStripMenuItem"
        Me.CậpNhậtThôngTinToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.CậpNhậtThôngTinToolStripMenuItem.Text = "Cập nhật thông tin"
        '
        'ThêmKháchSạnToolStripMenuItem
        '
        Me.ThêmKháchSạnToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KháchSạnToolStripMenuItem, Me.NhàHàngToolStripMenuItem, Me.PhươngTiệnDiChuyểnToolStripMenuItem})
        Me.ThêmKháchSạnToolStripMenuItem.Name = "ThêmKháchSạnToolStripMenuItem"
        Me.ThêmKháchSạnToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ThêmKháchSạnToolStripMenuItem.Text = "Thêm dịch vụ"
        '
        'KháchSạnToolStripMenuItem
        '
        Me.KháchSạnToolStripMenuItem.Name = "KháchSạnToolStripMenuItem"
        Me.KháchSạnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.KháchSạnToolStripMenuItem.Text = "Khách sạn"
        '
        'NhàHàngToolStripMenuItem
        '
        Me.NhàHàngToolStripMenuItem.Name = "NhàHàngToolStripMenuItem"
        Me.NhàHàngToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.NhàHàngToolStripMenuItem.Text = "Nhà hàng"
        '
        'PhươngTiệnDiChuyểnToolStripMenuItem
        '
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Name = "PhươngTiệnDiChuyểnToolStripMenuItem"
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Text = "Phương tiện di chuyển"
        '
        'XemChiTiếtTourToolStripMenuItem
        '
        Me.XemChiTiếtTourToolStripMenuItem.Name = "XemChiTiếtTourToolStripMenuItem"
        Me.XemChiTiếtTourToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.XemChiTiếtTourToolStripMenuItem.Text = "Xem chi tiết tour"
        '
        'QuảnLýToolStripMenuItem
        '
        Me.QuảnLýToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ThêmTourToolStripMenuItem, Me.XóaTourToolStripMenuItem})
        Me.QuảnLýToolStripMenuItem.Name = "QuảnLýToolStripMenuItem"
        Me.QuảnLýToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.QuảnLýToolStripMenuItem.Text = "Quản lý"
        '
        'ThêmTourToolStripMenuItem
        '
        Me.ThêmTourToolStripMenuItem.Name = "ThêmTourToolStripMenuItem"
        Me.ThêmTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ThêmTourToolStripMenuItem.Text = "Thêm Tour"
        '
        'XóaTourToolStripMenuItem
        '
        Me.XóaTourToolStripMenuItem.Name = "XóaTourToolStripMenuItem"
        Me.XóaTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.XóaTourToolStripMenuItem.Text = "Xóa Tour"
        '
        'frmDanhSachTour
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(694, 294)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmDanhSachTour"
        Me.Text = "Danh sách Tour"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Button2 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ChứcNăngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ĐặtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CậpNhậtThôngTinToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThêmKháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NhàHàngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PhươngTiệnDiChuyểnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents XemChiTiếtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThêmTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents XóaTourToolStripMenuItem As ToolStripMenuItem
End Class
