﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDanhSachNhanVien
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txthoten = New System.Windows.Forms.TextBox()
        Me.txtcmnd = New System.Windows.Forms.TextBox()
        Me.txtdiachi = New System.Windows.Forms.TextBox()
        Me.txtdienthoai = New System.Windows.Forms.TextBox()
        Me.txtemail = New System.Windows.Forms.TextBox()
        Me.txttendangnhap = New System.Windows.Forms.TextBox()
        Me.txtmatkhau = New System.Windows.Forms.TextBox()
        Me.dtplichnghi = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnthem = New System.Windows.Forms.Button()
        Me.btnsualn = New System.Windows.Forms.Button()
        Me.btncapnhat = New System.Windows.Forms.Button()
        Me.btntimkiem = New System.Windows.Forms.Button()
        Me.btndatlaimk = New System.Windows.Forms.Button()
        Me.btnthemln = New System.Windows.Forms.Button()
        Me.btnxoa = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(718, 218)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông Tin Nhân Viên"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 19)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(706, 188)
        Me.DataGridView1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 261)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Họ Tên"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 377)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Điện Thoại"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(252, 261)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Email"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 302)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "CMND"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(231, 343)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Mật Khẩu"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 343)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Địa Chỉ"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(208, 302)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Tên Đăng Nhập"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(231, 377)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Lịch Nghĩ"
        '
        'txthoten
        '
        Me.txthoten.Location = New System.Drawing.Point(92, 253)
        Me.txthoten.Name = "txthoten"
        Me.txthoten.Size = New System.Drawing.Size(110, 20)
        Me.txthoten.TabIndex = 2
        '
        'txtcmnd
        '
        Me.txtcmnd.Location = New System.Drawing.Point(92, 295)
        Me.txtcmnd.Name = "txtcmnd"
        Me.txtcmnd.Size = New System.Drawing.Size(110, 20)
        Me.txtcmnd.TabIndex = 2
        '
        'txtdiachi
        '
        Me.txtdiachi.Location = New System.Drawing.Point(92, 336)
        Me.txtdiachi.Name = "txtdiachi"
        Me.txtdiachi.Size = New System.Drawing.Size(110, 20)
        Me.txtdiachi.TabIndex = 2
        '
        'txtdienthoai
        '
        Me.txtdienthoai.Location = New System.Drawing.Point(92, 370)
        Me.txtdienthoai.Name = "txtdienthoai"
        Me.txtdienthoai.Size = New System.Drawing.Size(110, 20)
        Me.txtdienthoai.TabIndex = 2
        '
        'txtemail
        '
        Me.txtemail.Location = New System.Drawing.Point(301, 254)
        Me.txtemail.Name = "txtemail"
        Me.txtemail.Size = New System.Drawing.Size(135, 20)
        Me.txtemail.TabIndex = 2
        '
        'txttendangnhap
        '
        Me.txttendangnhap.Location = New System.Drawing.Point(301, 299)
        Me.txttendangnhap.Name = "txttendangnhap"
        Me.txttendangnhap.Size = New System.Drawing.Size(135, 20)
        Me.txttendangnhap.TabIndex = 2
        '
        'txtmatkhau
        '
        Me.txtmatkhau.Location = New System.Drawing.Point(301, 336)
        Me.txtmatkhau.Name = "txtmatkhau"
        Me.txtmatkhau.Size = New System.Drawing.Size(135, 20)
        Me.txtmatkhau.TabIndex = 2
        '
        'dtplichnghi
        '
        Me.dtplichnghi.Location = New System.Drawing.Point(301, 371)
        Me.dtplichnghi.Name = "dtplichnghi"
        Me.dtplichnghi.Size = New System.Drawing.Size(169, 20)
        Me.dtplichnghi.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnxoa)
        Me.GroupBox2.Controls.Add(Me.btnthemln)
        Me.GroupBox2.Controls.Add(Me.btndatlaimk)
        Me.GroupBox2.Controls.Add(Me.btntimkiem)
        Me.GroupBox2.Controls.Add(Me.btncapnhat)
        Me.GroupBox2.Controls.Add(Me.btnsualn)
        Me.GroupBox2.Controls.Add(Me.btnthem)
        Me.GroupBox2.Location = New System.Drawing.Point(504, 236)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(220, 155)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cập Nhật"
        '
        'btnthem
        '
        Me.btnthem.Location = New System.Drawing.Point(18, 25)
        Me.btnthem.Name = "btnthem"
        Me.btnthem.Size = New System.Drawing.Size(75, 23)
        Me.btnthem.TabIndex = 0
        Me.btnthem.Text = "Thêm"
        Me.btnthem.UseVisualStyleBackColor = True
        '
        'btnsualn
        '
        Me.btnsualn.Location = New System.Drawing.Point(117, 25)
        Me.btnsualn.Name = "btnsualn"
        Me.btnsualn.Size = New System.Drawing.Size(75, 23)
        Me.btnsualn.TabIndex = 0
        Me.btnsualn.Text = "Sửa LN"
        Me.btnsualn.UseVisualStyleBackColor = True
        '
        'btncapnhat
        '
        Me.btncapnhat.Location = New System.Drawing.Point(117, 54)
        Me.btncapnhat.Name = "btncapnhat"
        Me.btncapnhat.Size = New System.Drawing.Size(75, 23)
        Me.btncapnhat.TabIndex = 0
        Me.btncapnhat.Text = "Cập Nhật"
        Me.btncapnhat.UseVisualStyleBackColor = True
        '
        'btntimkiem
        '
        Me.btntimkiem.Location = New System.Drawing.Point(117, 86)
        Me.btntimkiem.Name = "btntimkiem"
        Me.btntimkiem.Size = New System.Drawing.Size(75, 23)
        Me.btntimkiem.TabIndex = 0
        Me.btntimkiem.Text = "Tìm Kiếm"
        Me.btntimkiem.UseVisualStyleBackColor = True
        '
        'btndatlaimk
        '
        Me.btndatlaimk.Location = New System.Drawing.Point(69, 124)
        Me.btndatlaimk.Name = "btndatlaimk"
        Me.btndatlaimk.Size = New System.Drawing.Size(75, 23)
        Me.btndatlaimk.TabIndex = 0
        Me.btndatlaimk.Text = "Đặt Lại MK"
        Me.btndatlaimk.UseVisualStyleBackColor = True
        '
        'btnthemln
        '
        Me.btnthemln.Location = New System.Drawing.Point(18, 60)
        Me.btnthemln.Name = "btnthemln"
        Me.btnthemln.Size = New System.Drawing.Size(75, 23)
        Me.btnthemln.TabIndex = 0
        Me.btnthemln.Text = "Thêm LN"
        Me.btnthemln.UseVisualStyleBackColor = True
        '
        'btnxoa
        '
        Me.btnxoa.Location = New System.Drawing.Point(18, 95)
        Me.btnxoa.Name = "btnxoa"
        Me.btnxoa.Size = New System.Drawing.Size(75, 23)
        Me.btnxoa.TabIndex = 0
        Me.btnxoa.Text = "Xóa"
        Me.btnxoa.UseVisualStyleBackColor = True
        '
        'DanhSachNhanVien
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(742, 399)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dtplichnghi)
        Me.Controls.Add(Me.txtmatkhau)
        Me.Controls.Add(Me.txttendangnhap)
        Me.Controls.Add(Me.txtemail)
        Me.Controls.Add(Me.txtdienthoai)
        Me.Controls.Add(Me.txtdiachi)
        Me.Controls.Add(Me.txtcmnd)
        Me.Controls.Add(Me.txthoten)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "DanhSachNhanVien"
        Me.Text = "DanhSachNhanVien"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txthoten As TextBox
    Friend WithEvents txtcmnd As TextBox
    Friend WithEvents txtdiachi As TextBox
    Friend WithEvents txtdienthoai As TextBox
    Friend WithEvents txtemail As TextBox
    Friend WithEvents txttendangnhap As TextBox
    Friend WithEvents txtmatkhau As TextBox
    Friend WithEvents dtplichnghi As DateTimePicker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnxoa As Button
    Friend WithEvents btnthemln As Button
    Friend WithEvents btndatlaimk As Button
    Friend WithEvents btntimkiem As Button
    Friend WithEvents btncapnhat As Button
    Friend WithEvents btnsualn As Button
    Friend WithEvents btnthem As Button
End Class
