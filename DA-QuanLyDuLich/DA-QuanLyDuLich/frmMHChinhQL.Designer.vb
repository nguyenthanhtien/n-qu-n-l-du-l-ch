﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMHChinhQL
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ChứcNăngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐặtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CậpNhậtThôngTinToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DịchVụToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChiTiếtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NhàHàngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PhươngTiệnDiChuyểnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TạoTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêThángToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêDoanhThuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêLợiNhuậnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChứcNăngToolStripMenuItem, Me.QuảnLýToolStripMenuItem, Me.ThốngKêToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(662, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ChứcNăngToolStripMenuItem
        '
        Me.ChứcNăngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ĐặtTourToolStripMenuItem, Me.CậpNhậtThôngTinToolStripMenuItem, Me.DịchVụToolStripMenuItem, Me.ChiTiếtTourToolStripMenuItem})
        Me.ChứcNăngToolStripMenuItem.Name = "ChứcNăngToolStripMenuItem"
        Me.ChứcNăngToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ChứcNăngToolStripMenuItem.Text = "Chức năng"
        '
        'ĐặtTourToolStripMenuItem
        '
        Me.ĐặtTourToolStripMenuItem.Name = "ĐặtTourToolStripMenuItem"
        Me.ĐặtTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ĐặtTourToolStripMenuItem.Text = "Đặt Tour"
        '
        'CậpNhậtThôngTinToolStripMenuItem
        '
        Me.CậpNhậtThôngTinToolStripMenuItem.Name = "CậpNhậtThôngTinToolStripMenuItem"
        Me.CậpNhậtThôngTinToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.CậpNhậtThôngTinToolStripMenuItem.Text = "Cập nhật thông tin"
        '
        'DịchVụToolStripMenuItem
        '
        Me.DịchVụToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NhàHàngToolStripMenuItem, Me.KháchSạnToolStripMenuItem, Me.PhươngTiệnDiChuyểnToolStripMenuItem})
        Me.DịchVụToolStripMenuItem.Name = "DịchVụToolStripMenuItem"
        Me.DịchVụToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.DịchVụToolStripMenuItem.Text = "Dịch vụ"
        '
        'ChiTiếtTourToolStripMenuItem
        '
        Me.ChiTiếtTourToolStripMenuItem.Name = "ChiTiếtTourToolStripMenuItem"
        Me.ChiTiếtTourToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ChiTiếtTourToolStripMenuItem.Text = "Chi tiết Tour"
        '
        'NhàHàngToolStripMenuItem
        '
        Me.NhàHàngToolStripMenuItem.Name = "NhàHàngToolStripMenuItem"
        Me.NhàHàngToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.NhàHàngToolStripMenuItem.Text = "Nhà hàng"
        '
        'KháchSạnToolStripMenuItem
        '
        Me.KháchSạnToolStripMenuItem.Name = "KháchSạnToolStripMenuItem"
        Me.KháchSạnToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.KháchSạnToolStripMenuItem.Text = "Khách Sạn"
        '
        'PhươngTiệnDiChuyểnToolStripMenuItem
        '
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Name = "PhươngTiệnDiChuyểnToolStripMenuItem"
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Text = "Phương tiện di chuyển"
        '
        'QuảnLýToolStripMenuItem
        '
        Me.QuảnLýToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TạoTourToolStripMenuItem, Me.QuảnLýTourToolStripMenuItem})
        Me.QuảnLýToolStripMenuItem.Name = "QuảnLýToolStripMenuItem"
        Me.QuảnLýToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.QuảnLýToolStripMenuItem.Text = "Quản lý"
        '
        'TạoTourToolStripMenuItem
        '
        Me.TạoTourToolStripMenuItem.Name = "TạoTourToolStripMenuItem"
        Me.TạoTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TạoTourToolStripMenuItem.Text = "Tạo Tour"
        '
        'QuảnLýTourToolStripMenuItem
        '
        Me.QuảnLýTourToolStripMenuItem.Name = "QuảnLýTourToolStripMenuItem"
        Me.QuảnLýTourToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.QuảnLýTourToolStripMenuItem.Text = "Quản lý Tour"
        '
        'ThốngKêToolStripMenuItem
        '
        Me.ThốngKêToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ThốngKêThángToolStripMenuItem, Me.ThốngKêDoanhThuToolStripMenuItem, Me.ThốngKêLợiNhuậnToolStripMenuItem})
        Me.ThốngKêToolStripMenuItem.Name = "ThốngKêToolStripMenuItem"
        Me.ThốngKêToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.ThốngKêToolStripMenuItem.Text = "Thống kê"
        '
        'ThốngKêThángToolStripMenuItem
        '
        Me.ThốngKêThángToolStripMenuItem.Name = "ThốngKêThángToolStripMenuItem"
        Me.ThốngKêThángToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ThốngKêThángToolStripMenuItem.Text = "Thống kê tháng"
        '
        'ThốngKêDoanhThuToolStripMenuItem
        '
        Me.ThốngKêDoanhThuToolStripMenuItem.Name = "ThốngKêDoanhThuToolStripMenuItem"
        Me.ThốngKêDoanhThuToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ThốngKêDoanhThuToolStripMenuItem.Text = "Thống kê doanh thu"
        '
        'ThốngKêLợiNhuậnToolStripMenuItem
        '
        Me.ThốngKêLợiNhuậnToolStripMenuItem.Name = "ThốngKêLợiNhuậnToolStripMenuItem"
        Me.ThốngKêLợiNhuậnToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ThốngKêLợiNhuậnToolStripMenuItem.Text = "Thống kê lợi nhuận"
        '
        'frmMHChinhQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(662, 384)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMHChinhQL"
        Me.Text = "frmMHChinhQL"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ChứcNăngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ĐặtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CậpNhậtThôngTinToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DịchVụToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NhàHàngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChiTiếtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PhươngTiệnDiChuyểnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TạoTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêThángToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêDoanhThuToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêLợiNhuậnToolStripMenuItem As ToolStripMenuItem
End Class
