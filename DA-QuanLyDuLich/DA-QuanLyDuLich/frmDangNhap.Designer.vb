﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDangNhap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lb_dangnhap = New System.Windows.Forms.Label()
        Me.lb_matkhau = New System.Windows.Forms.Label()
        Me.txt_dangnhap = New System.Windows.Forms.TextBox()
        Me.txt_matkhau = New System.Windows.Forms.TextBox()
        Me.btn_dangnhap = New System.Windows.Forms.Button()
        Me.btn_thoat = New System.Windows.Forms.Button()
        Me.lb_Quenmatkhau = New System.Windows.Forms.Label()
        Me.cb_loaitaikhoan = New System.Windows.Forms.ComboBox()
        Me.lb_chucvu = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lb_dangnhap
        '
        Me.lb_dangnhap.AutoSize = True
        Me.lb_dangnhap.Location = New System.Drawing.Point(29, 42)
        Me.lb_dangnhap.Name = "lb_dangnhap"
        Me.lb_dangnhap.Size = New System.Drawing.Size(58, 13)
        Me.lb_dangnhap.TabIndex = 0
        Me.lb_dangnhap.Text = "Username:"
        '
        'lb_matkhau
        '
        Me.lb_matkhau.AutoSize = True
        Me.lb_matkhau.Location = New System.Drawing.Point(31, 79)
        Me.lb_matkhau.Name = "lb_matkhau"
        Me.lb_matkhau.Size = New System.Drawing.Size(56, 13)
        Me.lb_matkhau.TabIndex = 1
        Me.lb_matkhau.Text = "Password:"
        '
        'txt_dangnhap
        '
        Me.txt_dangnhap.Location = New System.Drawing.Point(135, 39)
        Me.txt_dangnhap.Name = "txt_dangnhap"
        Me.txt_dangnhap.Size = New System.Drawing.Size(121, 20)
        Me.txt_dangnhap.TabIndex = 2
        '
        'txt_matkhau
        '
        Me.txt_matkhau.Location = New System.Drawing.Point(135, 76)
        Me.txt_matkhau.Name = "txt_matkhau"
        Me.txt_matkhau.Size = New System.Drawing.Size(121, 20)
        Me.txt_matkhau.TabIndex = 3
        '
        'btn_dangnhap
        '
        Me.btn_dangnhap.Location = New System.Drawing.Point(42, 197)
        Me.btn_dangnhap.Name = "btn_dangnhap"
        Me.btn_dangnhap.Size = New System.Drawing.Size(75, 23)
        Me.btn_dangnhap.TabIndex = 4
        Me.btn_dangnhap.Text = "Login"
        Me.btn_dangnhap.UseVisualStyleBackColor = True
        '
        'btn_thoat
        '
        Me.btn_thoat.Location = New System.Drawing.Point(164, 197)
        Me.btn_thoat.Name = "btn_thoat"
        Me.btn_thoat.Size = New System.Drawing.Size(75, 23)
        Me.btn_thoat.TabIndex = 5
        Me.btn_thoat.Text = "Cancel"
        Me.btn_thoat.UseVisualStyleBackColor = True
        '
        'lb_Quenmatkhau
        '
        Me.lb_Quenmatkhau.AutoSize = True
        Me.lb_Quenmatkhau.Location = New System.Drawing.Point(94, 156)
        Me.lb_Quenmatkhau.Name = "lb_Quenmatkhau"
        Me.lb_Quenmatkhau.Size = New System.Drawing.Size(89, 13)
        Me.lb_Quenmatkhau.TabIndex = 7
        Me.lb_Quenmatkhau.Text = "Quên mật khẩu ?"
        '
        'cb_loaitaikhoan
        '
        Me.cb_loaitaikhoan.FormattingEnabled = True
        Me.cb_loaitaikhoan.Items.AddRange(New Object() {"Khách Hàng", "Quản Lý", "Nhân Viên"})
        Me.cb_loaitaikhoan.Location = New System.Drawing.Point(135, 117)
        Me.cb_loaitaikhoan.Name = "cb_loaitaikhoan"
        Me.cb_loaitaikhoan.Size = New System.Drawing.Size(121, 21)
        Me.cb_loaitaikhoan.TabIndex = 8
        '
        'lb_chucvu
        '
        Me.lb_chucvu.AutoSize = True
        Me.lb_chucvu.Location = New System.Drawing.Point(31, 120)
        Me.lb_chucvu.Name = "lb_chucvu"
        Me.lb_chucvu.Size = New System.Drawing.Size(77, 13)
        Me.lb_chucvu.TabIndex = 9
        Me.lb_chucvu.Text = "Loại tài khoản:"
        '
        'frmDangNhap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.lb_chucvu)
        Me.Controls.Add(Me.cb_loaitaikhoan)
        Me.Controls.Add(Me.lb_Quenmatkhau)
        Me.Controls.Add(Me.btn_thoat)
        Me.Controls.Add(Me.btn_dangnhap)
        Me.Controls.Add(Me.txt_matkhau)
        Me.Controls.Add(Me.txt_dangnhap)
        Me.Controls.Add(Me.lb_matkhau)
        Me.Controls.Add(Me.lb_dangnhap)
        Me.Name = "frmDangNhap"
        Me.Text = "Đăng Nhập"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lb_dangnhap As Label
    Friend WithEvents lb_matkhau As Label
    Friend WithEvents txt_dangnhap As TextBox
    Friend WithEvents txt_matkhau As TextBox
    Friend WithEvents btn_dangnhap As Button
    Friend WithEvents btn_thoat As Button
    Friend WithEvents lb_Quenmatkhau As Label
    Friend WithEvents cb_loaitaikhoan As ComboBox
    Friend WithEvents lb_chucvu As Label
End Class
