﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLichHDV
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ChứcNăngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐặtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThôngTinChiTiếtToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DịchVụToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KháchSạnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NhàHàngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PhươngTiệnDiChuyểnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChiTiếtTourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MonthCalendar1
        '
        Me.MonthCalendar1.Location = New System.Drawing.Point(18, 40)
        Me.MonthCalendar1.Name = "MonthCalendar1"
        Me.MonthCalendar1.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(18, 284)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(621, 142)
        Me.DataGridView1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(288, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Ngày làm việc :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(288, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Mã Tour :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(288, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tên tour :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(288, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Thời gian :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(288, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Tóm tắt chương trình :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(288, 176)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Khách sạn :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(288, 200)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Quán ăn :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(288, 227)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Phương tiện :"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(405, 31)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 4
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(405, 60)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 4
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(405, 90)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 20)
        Me.TextBox3.TabIndex = 4
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(405, 118)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 20)
        Me.TextBox4.TabIndex = 4
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(405, 143)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(100, 20)
        Me.TextBox5.TabIndex = 4
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(405, 169)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox6.TabIndex = 4
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(405, 193)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 20)
        Me.TextBox7.TabIndex = 4
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(405, 220)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(100, 20)
        Me.TextBox8.TabIndex = 4
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChứcNăngToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(678, 24)
        Me.MenuStrip1.TabIndex = 5
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ChứcNăngToolStripMenuItem
        '
        Me.ChứcNăngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ĐặtTourToolStripMenuItem, Me.ThôngTinChiTiếtToolStripMenuItem, Me.DịchVụToolStripMenuItem, Me.ChiTiếtTourToolStripMenuItem})
        Me.ChứcNăngToolStripMenuItem.Name = "ChứcNăngToolStripMenuItem"
        Me.ChứcNăngToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ChứcNăngToolStripMenuItem.Text = "Chức năng"
        '
        'ĐặtTourToolStripMenuItem
        '
        Me.ĐặtTourToolStripMenuItem.Name = "ĐặtTourToolStripMenuItem"
        Me.ĐặtTourToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ĐặtTourToolStripMenuItem.Text = "Đặt tour"
        '
        'ThôngTinChiTiếtToolStripMenuItem
        '
        Me.ThôngTinChiTiếtToolStripMenuItem.Name = "ThôngTinChiTiếtToolStripMenuItem"
        Me.ThôngTinChiTiếtToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ThôngTinChiTiếtToolStripMenuItem.Text = "Thông tin chi tiết"
        '
        'DịchVụToolStripMenuItem
        '
        Me.DịchVụToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KháchSạnToolStripMenuItem, Me.NhàHàngToolStripMenuItem, Me.PhươngTiệnDiChuyểnToolStripMenuItem})
        Me.DịchVụToolStripMenuItem.Name = "DịchVụToolStripMenuItem"
        Me.DịchVụToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.DịchVụToolStripMenuItem.Text = "Dịch vụ"
        '
        'KháchSạnToolStripMenuItem
        '
        Me.KháchSạnToolStripMenuItem.Name = "KháchSạnToolStripMenuItem"
        Me.KháchSạnToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.KháchSạnToolStripMenuItem.Text = "Khách sạn"
        '
        'NhàHàngToolStripMenuItem
        '
        Me.NhàHàngToolStripMenuItem.Name = "NhàHàngToolStripMenuItem"
        Me.NhàHàngToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.NhàHàngToolStripMenuItem.Text = "Nhà hàng"
        '
        'PhươngTiệnDiChuyểnToolStripMenuItem
        '
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Name = "PhươngTiệnDiChuyểnToolStripMenuItem"
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.PhươngTiệnDiChuyểnToolStripMenuItem.Text = "Phương tiện di chuyển"
        '
        'ChiTiếtTourToolStripMenuItem
        '
        Me.ChiTiếtTourToolStripMenuItem.Name = "ChiTiếtTourToolStripMenuItem"
        Me.ChiTiếtTourToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ChiTiếtTourToolStripMenuItem.Text = "Chi tiết Tour"
        '
        'frmLichHDV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(678, 452)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.MonthCalendar1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmLichHDV"
        Me.Text = "Lịch hướng dẫn viên"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MonthCalendar1 As MonthCalendar
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ChứcNăngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ĐặtTourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThôngTinChiTiếtToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DịchVụToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KháchSạnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NhàHàngToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PhươngTiệnDiChuyểnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChiTiếtTourToolStripMenuItem As ToolStripMenuItem
End Class
