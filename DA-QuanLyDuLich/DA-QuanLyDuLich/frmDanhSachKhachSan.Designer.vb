﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDanhSachKhachSan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgv_danhsachkhachsan = New System.Windows.Forms.DataGridView()
        Me.lb_ten = New System.Windows.Forms.Label()
        Me.lb_diachi = New System.Windows.Forms.Label()
        Me.lb_sao = New System.Windows.Forms.Label()
        Me.lb_quocgia = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lb_them1nguoi = New System.Windows.Forms.Label()
        Me.txt_ten = New System.Windows.Forms.TextBox()
        Me.txx_diachi = New System.Windows.Forms.TextBox()
        Me.cb_sao = New System.Windows.Forms.ComboBox()
        Me.cb_quocgia = New System.Windows.Forms.ComboBox()
        Me.txt_themnguoilon = New System.Windows.Forms.TextBox()
        Me.txt_themtreem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_them = New System.Windows.Forms.Button()
        Me.btn_xoa = New System.Windows.Forms.Button()
        Me.btn_sua = New System.Windows.Forms.Button()
        Me.cb_timkiem = New System.Windows.Forms.ComboBox()
        Me.txt_timkiem = New System.Windows.Forms.TextBox()
        Me.lb_timkiem = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgv_danhsachkhachsan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgv_danhsachkhachsan)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 48)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(637, 175)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Danh sách khách sạn"
        '
        'dgv_danhsachkhachsan
        '
        Me.dgv_danhsachkhachsan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_danhsachkhachsan.Location = New System.Drawing.Point(6, 19)
        Me.dgv_danhsachkhachsan.Name = "dgv_danhsachkhachsan"
        Me.dgv_danhsachkhachsan.Size = New System.Drawing.Size(625, 150)
        Me.dgv_danhsachkhachsan.TabIndex = 0
        '
        'lb_ten
        '
        Me.lb_ten.AutoSize = True
        Me.lb_ten.Location = New System.Drawing.Point(40, 249)
        Me.lb_ten.Name = "lb_ten"
        Me.lb_ten.Size = New System.Drawing.Size(32, 13)
        Me.lb_ten.TabIndex = 1
        Me.lb_ten.Text = "Tên :"
        '
        'lb_diachi
        '
        Me.lb_diachi.AutoSize = True
        Me.lb_diachi.Location = New System.Drawing.Point(40, 278)
        Me.lb_diachi.Name = "lb_diachi"
        Me.lb_diachi.Size = New System.Drawing.Size(46, 13)
        Me.lb_diachi.TabIndex = 2
        Me.lb_diachi.Text = "Địa chỉ :"
        '
        'lb_sao
        '
        Me.lb_sao.AutoSize = True
        Me.lb_sao.Location = New System.Drawing.Point(40, 306)
        Me.lb_sao.Name = "lb_sao"
        Me.lb_sao.Size = New System.Drawing.Size(32, 13)
        Me.lb_sao.TabIndex = 3
        Me.lb_sao.Text = "Sao :"
        '
        'lb_quocgia
        '
        Me.lb_quocgia.AutoSize = True
        Me.lb_quocgia.Location = New System.Drawing.Point(354, 309)
        Me.lb_quocgia.Name = "lb_quocgia"
        Me.lb_quocgia.Size = New System.Drawing.Size(88, 13)
        Me.lb_quocgia.TabIndex = 4
        Me.lb_quocgia.Text = "Quốc gia / Tỉnh :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(353, 278)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Thêm 1 trẻ em :"
        '
        'lb_them1nguoi
        '
        Me.lb_them1nguoi.AutoSize = True
        Me.lb_them1nguoi.Location = New System.Drawing.Point(353, 249)
        Me.lb_them1nguoi.Name = "lb_them1nguoi"
        Me.lb_them1nguoi.Size = New System.Drawing.Size(95, 13)
        Me.lb_them1nguoi.TabIndex = 6
        Me.lb_them1nguoi.Text = "Thêm 1 người lớn :"
        '
        'txt_ten
        '
        Me.txt_ten.Location = New System.Drawing.Point(130, 246)
        Me.txt_ten.Name = "txt_ten"
        Me.txt_ten.Size = New System.Drawing.Size(100, 20)
        Me.txt_ten.TabIndex = 7
        '
        'txx_diachi
        '
        Me.txx_diachi.Location = New System.Drawing.Point(130, 275)
        Me.txx_diachi.Name = "txx_diachi"
        Me.txx_diachi.Size = New System.Drawing.Size(100, 20)
        Me.txx_diachi.TabIndex = 8
        '
        'cb_sao
        '
        Me.cb_sao.FormattingEnabled = True
        Me.cb_sao.Items.AddRange(New Object() {"1", "2", "3", "4", "5"})
        Me.cb_sao.Location = New System.Drawing.Point(130, 303)
        Me.cb_sao.Name = "cb_sao"
        Me.cb_sao.Size = New System.Drawing.Size(121, 21)
        Me.cb_sao.TabIndex = 9
        '
        'cb_quocgia
        '
        Me.cb_quocgia.FormattingEnabled = True
        Me.cb_quocgia.Location = New System.Drawing.Point(497, 306)
        Me.cb_quocgia.Name = "cb_quocgia"
        Me.cb_quocgia.Size = New System.Drawing.Size(121, 21)
        Me.cb_quocgia.TabIndex = 10
        '
        'txt_themnguoilon
        '
        Me.txt_themnguoilon.Location = New System.Drawing.Point(497, 246)
        Me.txt_themnguoilon.Name = "txt_themnguoilon"
        Me.txt_themnguoilon.Size = New System.Drawing.Size(100, 20)
        Me.txt_themnguoilon.TabIndex = 11
        '
        'txt_themtreem
        '
        Me.txt_themtreem.Location = New System.Drawing.Point(497, 272)
        Me.txt_themtreem.Name = "txt_themtreem"
        Me.txt_themtreem.Size = New System.Drawing.Size(100, 20)
        Me.txt_themtreem.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(43, 337)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Giá :"
        '
        'btn_them
        '
        Me.btn_them.Location = New System.Drawing.Point(85, 379)
        Me.btn_them.Name = "btn_them"
        Me.btn_them.Size = New System.Drawing.Size(75, 23)
        Me.btn_them.TabIndex = 14
        Me.btn_them.Text = "Thêm"
        Me.btn_them.UseVisualStyleBackColor = True
        '
        'btn_xoa
        '
        Me.btn_xoa.Location = New System.Drawing.Point(297, 379)
        Me.btn_xoa.Name = "btn_xoa"
        Me.btn_xoa.Size = New System.Drawing.Size(75, 23)
        Me.btn_xoa.TabIndex = 15
        Me.btn_xoa.Text = "Xóa"
        Me.btn_xoa.UseVisualStyleBackColor = True
        '
        'btn_sua
        '
        Me.btn_sua.Location = New System.Drawing.Point(497, 379)
        Me.btn_sua.Name = "btn_sua"
        Me.btn_sua.Size = New System.Drawing.Size(75, 23)
        Me.btn_sua.TabIndex = 16
        Me.btn_sua.Text = "Sửa"
        Me.btn_sua.UseVisualStyleBackColor = True
        '
        'cb_timkiem
        '
        Me.cb_timkiem.FormattingEnabled = True
        Me.cb_timkiem.Location = New System.Drawing.Point(415, 12)
        Me.cb_timkiem.Name = "cb_timkiem"
        Me.cb_timkiem.Size = New System.Drawing.Size(121, 21)
        Me.cb_timkiem.TabIndex = 17
        '
        'txt_timkiem
        '
        Me.txt_timkiem.Location = New System.Drawing.Point(554, 13)
        Me.txt_timkiem.Name = "txt_timkiem"
        Me.txt_timkiem.Size = New System.Drawing.Size(100, 20)
        Me.txt_timkiem.TabIndex = 18
        '
        'lb_timkiem
        '
        Me.lb_timkiem.AutoSize = True
        Me.lb_timkiem.Location = New System.Drawing.Point(353, 16)
        Me.lb_timkiem.Name = "lb_timkiem"
        Me.lb_timkiem.Size = New System.Drawing.Size(49, 13)
        Me.lb_timkiem.TabIndex = 19
        Me.lb_timkiem.Text = "Tìm kiếm"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(130, 337)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Label3"
        '
        'frmDanhSachKhachSan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 416)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lb_timkiem)
        Me.Controls.Add(Me.txt_timkiem)
        Me.Controls.Add(Me.cb_timkiem)
        Me.Controls.Add(Me.btn_sua)
        Me.Controls.Add(Me.btn_xoa)
        Me.Controls.Add(Me.btn_them)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_themtreem)
        Me.Controls.Add(Me.txt_themnguoilon)
        Me.Controls.Add(Me.cb_quocgia)
        Me.Controls.Add(Me.cb_sao)
        Me.Controls.Add(Me.txx_diachi)
        Me.Controls.Add(Me.txt_ten)
        Me.Controls.Add(Me.lb_them1nguoi)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lb_quocgia)
        Me.Controls.Add(Me.lb_sao)
        Me.Controls.Add(Me.lb_diachi)
        Me.Controls.Add(Me.lb_ten)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmDanhSachKhachSan"
        Me.Text = "Danh sách khách sạn"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgv_danhsachkhachsan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgv_danhsachkhachsan As DataGridView
    Friend WithEvents lb_ten As Label
    Friend WithEvents lb_diachi As Label
    Friend WithEvents lb_sao As Label
    Friend WithEvents lb_quocgia As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lb_them1nguoi As Label
    Friend WithEvents txt_ten As TextBox
    Friend WithEvents txx_diachi As TextBox
    Friend WithEvents cb_sao As ComboBox
    Friend WithEvents cb_quocgia As ComboBox
    Friend WithEvents txt_themnguoilon As TextBox
    Friend WithEvents txt_themtreem As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_them As Button
    Friend WithEvents btn_xoa As Button
    Friend WithEvents btn_sua As Button
    Friend WithEvents cb_timkiem As ComboBox
    Friend WithEvents txt_timkiem As TextBox
    Friend WithEvents lb_timkiem As Label
    Friend WithEvents Label3 As Label
End Class
